import { Component } from '@angular/core';

interface Pessoa {
  id: number;
  nome: string;
  salario: number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nomes: string[] = ['Rick', 'Morty', 'Betty', 'Scrotian', 'Jim']

  filtered: string[] = this.nomes

  pessoasFiltradas: Array<Pessoa>
  retorno: Pessoa

  pessoas: Array<Pessoa> = [
    {
      id: 1,
      nome: 'Rick',
      salario: 1000, 
    },
    {
      id: 2,
      nome: 'Morty',
      salario: 119, 
    },
    {
      id: 3,
      nome: 'Betty',
      salario: 899, 
    },
  ]

  searchForValue(value: string) {
    this.filtered = this.nomes.filter((name: string) => name.toLowerCase().includes(value.toLowerCase()))
  }

  getValorTotal() {
    return this.pessoas.reduce((soma, pessoa) => soma + pessoa.salario, 0)
  }

  buscarPessoaPorId(id: number) {
    return this.pessoas.find(pessoa => pessoa.id == id)
  }

  aumentarSalario() {
    this.pessoas.forEach(pessoa => pessoa.salario = pessoa.salario * 2)
  }

  todosGanhamMaisQue500() {
    return this.pessoas.every(pessoa => pessoa.salario > 500)
  }

  buscaGenerica(value: any) {
    this.pessoasFiltradas = this.pessoas.filter(pessoa => {
      return Object.keys(pessoa).some(key => pessoa[key].toString().toLowerCase().includes(value))
    })
  }
}
